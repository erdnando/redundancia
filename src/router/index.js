import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from '../views/Home.vue'
import NuevoAnillo from '../views/NuevoAnillo.vue'
import Anillos from '../views/Anillos.vue'
import Ejecucion from '../views/Ejecucion.vue'
import Roles from '../views/Roles.vue'
import Historial from '../views/Historial.vue'


Vue.use(VueRouter);
Vue.prototype.$eventHub = new Vue();

const routes = [
  {
    path: '/',
    name: 'home',
    component: Home
  },
  {
    path: '/about',
    name: 'about',
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: () => import(/* webpackChunkName: "about" */ '../views/About.vue')
  },
  {
    path: '/nuevo-anillo',
    name: 'nuevo-anillo',
    component: NuevoAnillo
  },
  {
    path: '/anillos',
    name: 'anillos',
    component: Anillos
  },
  {
    path: '/ejecucion',
    name: 'ejecucion',
    component: Ejecucion
  },
  {
    path: '/historial',
    name: 'historial',
    component: Historial
  },
  {
    path: '/roles',
    name: 'roles',
    component: Roles
  },
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

export default router
