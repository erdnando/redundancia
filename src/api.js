import axios from 'axios';

export default() => {
    return axios.create({
        baseURL: `http://74.208.27.106:9080/rings-api`,
        withCredentials: false,
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json'
        }
    })
}