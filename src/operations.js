import Api from '../src/api';

export default {
    getWord (params) {
        return Api().get('/wordwise/entries?limit=1&headword=');
    },
    ejemplo(params){
        try{
            return Api().post("catalog/roles", {
                  email: params.correo,
                  password: params.contrasenia,
              });
        }
        catch(error){
            //debugger;
            console.log(error);
        } 
    },
    roles(params){
        try{
            return Api().get("catalog/roles");
        }
        catch(error){
            //debugger;
            console.log(error.response);
        } 
    },
    usersByRole(params){
        try{
            return Api().post("admin/usersbyrole", {
                  role: '',
              });
        }
        catch(error){
            //debugger;
            console.log(error);
        } 
    },
    assignrole(params){
        try{
            return Api().post("admin/assignrole", {
                  role: params.role,
                  username:params.username,
                  nombreCompleto:params.nombre
              });
        }
        catch(error){
            //debugger;
            console.log(error);
        } 
    },

}