import Vue from 'vue';
import Vuex from 'vuex';

Vue.use(Vuex);

export default new Vuex.Store({
  state: {
    anilloSeleccionado:0,
    templateXML:[
      {noAnillo: 2, xml:''},
      {noAnillo: 3, xml:''},
      {noAnillo: 4, xml:'<mxGraphModel dx="1422" dy="812" grid="1" gridSize="10" guides="1" tooltips="1" connect="1" arrows="1" fold="1" page="1" pageScale="1" pageWidth="827" pageHeight="1169">'+
      '<root>'+
        '<mxCell id="0"/>'+
        '<mxCell id="1" parent="0"/>'+
        '<mxCell id="4" class="nodo nodo1" value="nodo 1" style="shape=image;html=1;labelBackgroundColor=#ffffff;image=/edt/stencils/clipart/Router_Icon2.png;spacingTop=69;" parent="1" vertex="1">'+
          '<mxGeometry x="146" y="336" width="50" height="60" as="geometry"/>'+
        '</mxCell>'+
        '<mxCell id="2" class="nodo nodo2" value="nodo 2" style="shape=image;html=1;labelBackgroundColor=#ffffff;image=/edt/stencils/clipart/Router_Icon2.png;spacingTop=70;spacingLeft=40" parent="1" vertex="1">'+
          '<mxGeometry x="246" y="116" width="50" height="60" as="geometry"/>'+
        '</mxCell>'+
        '<mxCell id="3" class="nodo nodo3" value="nodo 3" style="shape=image;html=1;labelBackgroundColor=#ffffff;image=/edt/stencils/clipart/Router_Icon2.png;spacingTop=60;spacingLeft=-8;" parent="1" vertex="1">'+
          '<mxGeometry x="463" y="180" width="50" height="60" as="geometry"/>'+
        '</mxCell>'+
        '<mxCell id="5" class="nodo nodo4" value="nodo 4" style="shape=image;html=1;labelBackgroundColor=#ffffff;image=/edt/stencils/clipart/Router_Icon2.png;spacingRight=-30;spacingBottom=-80;spacingTop=-17;spacingLeft=-11;" parent="1" vertex="1">'+
          '<mxGeometry x="426" y="396" width="50" height="60" as="geometry"/>'+
        '</mxCell>'+
        '<mxCell id="6" class="lineaConec lineaConec_n1-n2" value="" style="edgeStyle=isometricEdgeStyle;endArrow=none;html=1;strokeColor=#B3B3B3;strokeWidth=4;exitX=0.75;exitY=1;exitDx=0;exitDy=0;" parent="1" source="62" target="2" edge="1">'+
          '<mxGeometry width="50" height="100" relative="1" as="geometry">'+
            '<mxPoint x="156" y="356" as="sourcePoint"/>'+
            '<mxPoint x="266" y="156" as="targetPoint"/>'+
            '<Array as="points">'+
              '<mxPoint x="110" y="330"/>'+
            '</Array>'+
          '</mxGeometry>'+
        '</mxCell>'+
        '<mxCell id="49" class="txtlineaConec txtlineaConec_n1-n2" value="N1-N2" style="text;html=1;align=center;verticalAlign=middle;resizable=0;points=[];labelBackgroundColor=#ffffff;" parent="6" vertex="1" connectable="0">'+
          '<mxGeometry x="-0.0451" relative="1" as="geometry">'+
            '<mxPoint x="1" as="offset"/>'+
          '</mxGeometry>'+
        '</mxCell>'+
        '<mxCell id="7" class="lineaConec lineaConec_n3-n4" value="" style="edgeStyle=isometricEdgeStyle;endArrow=none;html=1;exitX=1;exitY=0.25;exitDx=0;exitDy=0;strokeWidth=4;strokeColor=#B3B3B3;entryX=1;entryY=0.75;entryDx=0;entryDy=0;" parent="1" source="5" target="3" edge="1">'+
          '<mxGeometry width="50" height="100" relative="1" as="geometry">'+
            '<mxPoint x="486" y="296" as="sourcePoint"/>'+
            '<mxPoint x="590" y="240" as="targetPoint"/>'+
            '<Array as="points">'+
              '<mxPoint x="590" y="270"/>'+
            '</Array>'+
          '</mxGeometry>'+
        '</mxCell>'+
        '<mxCell id="51" class="txtlineaConec txtlineaConec_n3-n4" value="N3-N4" style="text;html=1;align=center;verticalAlign=middle;resizable=0;points=[];labelBackgroundColor=#ffffff;" parent="7" vertex="1" connectable="0">'+
          '<mxGeometry x="0.443" y="1" relative="1" as="geometry">'+
            '<mxPoint as="offset"/>'+
          '</mxGeometry>'+
        '</mxCell>'+
        '<mxCell id="8" class="lineaConec lineaConec_n2-n3" value="" style="edgeStyle=isometricEdgeStyle;endArrow=none;html=1;elbow=vertical;exitX=0;exitY=0.25;exitDx=0;exitDy=0;strokeWidth=4;strokeColor=#B3B3B3;" parent="1" source="3" edge="1">'+
          '<mxGeometry width="50" height="100" relative="1" as="geometry">'+
            '<mxPoint x="216" y="566" as="sourcePoint"/>'+
            '<mxPoint x="290" y="130" as="targetPoint"/>'+
            '<Array as="points">'+
              '<mxPoint x="310" y="120"/>'+
            '</Array>'+
          '</mxGeometry>'+
        '</mxCell>'+
        '<mxCell id="50" class="txtlineaConec txtlineaConec_n1-n2" value="N2-N3" style="text;html=1;align=center;verticalAlign=middle;resizable=0;points=[];labelBackgroundColor=#ffffff;" parent="8" vertex="1" connectable="0">'+
          '<mxGeometry x="-0.1987" y="4" relative="1" as="geometry">'+
            '<mxPoint as="offset"/>'+
          '</mxGeometry>'+
        '</mxCell>'+
        '<mxCell id="9" class="lineaConec lineaConec_n4-n1" value="" style="edgeStyle=isometricEdgeStyle;endArrow=none;html=1;elbow=vertical;strokeWidth=4;strokeColor=#B3B3B3;" parent="1" source="5" target="4" edge="1">'+
          '<mxGeometry width="50" height="100" relative="1" as="geometry">'+
            '<mxPoint x="236" y="546" as="sourcePoint"/>'+
            '<mxPoint x="286" y="446" as="targetPoint"/>'+
            '<Array as="points">'+
              '<mxPoint x="350" y="420"/>'+
            '</Array>'+
          '</mxGeometry>'+
        '</mxCell>'+
        '<mxCell id="52" class="txtlineaConec txtlineaConec_n1-n2" value="N4-N1" style="text;html=1;align=center;verticalAlign=middle;resizable=0;points=[];labelBackgroundColor=#ffffff;" parent="9" vertex="1" connectable="0">'+
          '<mxGeometry x="-0.0099" y="-2" relative="1" as="geometry">'+
            '<mxPoint x="-1" as="offset"/>'+
          '</mxGeometry>'+
        '</mxCell>'+
        '<mxCell id="32" class="OLT OLT1" value="OLT1" style="shape=image;html=1;labelBackgroundColor=#ffffff;image=/edt/stencils/clipart/Earth_globe_128x128.png;spacingTop=0;spacingLeft=0;" parent="1" vertex="1">'+
          '<mxGeometry x="76" y="385" width="50" height="40" as="geometry"/>'+
        '</mxCell>'+
        '<mxCell id="37" class="LNOLT LNOLT1" value="o1" style="rounded=0;comic=1;strokeWidth=4;endArrow=none;html=1;fontFamily=Comic Sans MS;fontStyle=1;endFill=0;strokeColor=#B3B3B3;" parent="1" source="4" target="32" edge="1">'+
          '<mxGeometry width="50" height="50" relative="1" as="geometry">'+
            '<mxPoint x="-54" y="376" as="sourcePoint"/>'+
            '<mxPoint x="-4" y="326" as="targetPoint"/>'+
          '</mxGeometry>'+
        '</mxCell>'+
        '<mxCell id="38" class="LNOLT LNOLT3" value="o3" style="rounded=0;comic=1;strokeWidth=4;endArrow=none;html=1;fontFamily=Comic Sans MS;fontStyle=1;endFill=0;strokeColor=#B3B3B3;" parent="1" source="40" target="3" edge="1">'+
          '<mxGeometry width="50" height="50" relative="1" as="geometry">'+
            '<mxPoint x="533" y="240.0042857142858" as="sourcePoint"/>'+
            '<mxPoint x="513" y="228.57571428571424" as="targetPoint"/>'+
          '</mxGeometry>'+
        '</mxCell>'+
        '<mxCell id="40" class="OLT OLT3" value="OLT3" style="shape=image;html=1;labelBackgroundColor=#ffffff;image=/edt/stencils/clipart/Earth_globe_128x128.png;spacingTop=-34;spacingRight=0;spacingLeft=75;" parent="1" vertex="1">'+
          '<mxGeometry x="540" y="136" width="50" height="40" as="geometry"/>'+
        '</mxCell>'+
        '<mxCell id="41" class="LNOLT LNOLT4" value="o4" style="rounded=0;comic=1;strokeWidth=4;endArrow=none;html=1;fontFamily=Comic Sans MS;fontStyle=1;endFill=0;strokeColor=#B3B3B3;" parent="1" source="43" edge="1">'+
          '<mxGeometry width="50" height="50" relative="1" as="geometry">'+
            '<mxPoint x="406" y="456" as="sourcePoint"/>'+
            '<mxPoint x="460" y="441" as="targetPoint"/>'+
          '</mxGeometry>'+
        '</mxCell>'+
        '<mxCell id="42" class="LNOLT LNOLT2" value="o2" style="rounded=0;comic=1;strokeWidth=4;endArrow=none;html=1;fontFamily=Comic Sans MS;fontStyle=1;endFill=0;strokeColor=#B3B3B3;" parent="1" source="44" edge="1">'+
          '<mxGeometry width="50" height="50" relative="1" as="geometry">'+
            '<mxPoint x="326" y="106" as="sourcePoint"/>'+
            '<mxPoint x="250" y="130" as="targetPoint"/>'+
          '</mxGeometry>'+
        '</mxCell>'+
        '<mxCell id="43" class="OLT OLT4" value="OLT4" style="shape=image;html=1;labelBackgroundColor=#ffffff;image=/edt/stencils/clipart/Earth_globe_128x128.png" parent="1" vertex="1">'+
          '<mxGeometry x="500" y="466" width="50" height="40" as="geometry"/>'+
        '</mxCell>'+
        '<mxCell id="44" class="OLT OLT2" value="OLT2" style="shape=image;html=1;labelBackgroundColor=#ffffff;image=/edt/stencils/clipart/Earth_globe_128x128.png;spacingTop=-34;spacingLeft=75;" parent="1" vertex="1">'+
          '<mxGeometry x="172" y="76" width="50" height="40" as="geometry"/>'+
        '</mxCell>'+
        '<mxCell id="54" class="Xcon Xcon3" value="x3" style="shape=image;html=1;labelBackgroundColor=#ffffff;image=/edt/stencils/clipart/Corte.png;" parent="1" vertex="1">'+
          '<mxGeometry x="232" y="148" width="20" height="18" as="geometry"/>'+
        '</mxCell>'+
        '<mxCell id="55" class="Xcon Xcon4" value="x4" style="shape=image;html=1;labelBackgroundColor=#ffffff;image=/edt/stencils/clipart/Corte.png;" parent="1" vertex="1">'+
          '<mxGeometry x="292" y="117" width="20" height="18" as="geometry"/>'+
        '</mxCell>'+
        '<mxCell id="56" class="Xcon Xcon1" value="x1" style="shape=image;html=1;labelBackgroundColor=#ffffff;image=/edt/stencils/clipart/Corte.png;" parent="1" vertex="1">'+
          '<mxGeometry x="123" y="336" width="20" height="18" as="geometry"/>'+
        '</mxCell>'+
        '<mxCell id="57" class="Xcon Xcon2" value="x2" style="shape=image;html=1;labelBackgroundColor=#ffffff;image=/edt/stencils/clipart/Corte.png;" parent="1" vertex="1">'+
          '<mxGeometry x="195" y="376" width="20" height="18" as="geometry"/>'+
        '</mxCell>'+
        '<mxCell id="58" class="Xcon Xcon5" value="x5" style="shape=image;html=1;labelBackgroundColor=#ffffff;image=/edt/stencils/clipart/Corte.png;" parent="1" vertex="1">'+
          '<mxGeometry x="445" y="180" width="20" height="18" as="geometry"/>'+
        '</mxCell>'+
        '<mxCell id="59" class="Xcon Xcon6" value="x6" style="shape=image;html=1;labelBackgroundColor=#ffffff;image=/edt/stencils/clipart/Corte.png;" parent="1" vertex="1">'+
          '<mxGeometry x="506" y="214" width="20" height="18" as="geometry"/>'+
        '</mxCell>'+
        '<mxCell id="60" class="Xcon Xcon7" value="x7" style="shape=image;html=1;labelBackgroundColor=#ffffff;image=/edt/stencils/clipart/Corte.png;" parent="1" vertex="1">'+
          '<mxGeometry x="473" y="396" width="20" height="18" as="geometry"/>'+
        '</mxCell>'+
        '<mxCell id="61" class="Xcon Xcon8" value="x8" style="shape=image;html=1;labelBackgroundColor=#ffffff;image=/edt/stencils/clipart/Corte.png;" parent="1" vertex="1">'+
          '<mxGeometry x="410" y="398" width="20" height="18" as="geometry"/>'+
        '</mxCell>'+
        '<mxCell id="62" class="ITF ITF1_N1" value="interfaz1" style="text;strokeColor=none;fillColor=none;html=1;fontSize=13;fontStyle=0;verticalAlign=middle;align=center;fontColor=#FF0000;" parent="1" vertex="1">'+
          '<mxGeometry x="65" y="300" width="100" height="40" as="geometry"/>'+
        '</mxCell>'+
        '<mxCell id="63" class="ITF ITF2_N1" value="interfaz2" style="text;strokeColor=none;fillColor=none;html=1;fontSize=13;fontStyle=0;verticalAlign=middle;align=center;fontColor=#FF0000;" parent="1" vertex="1">'+
          '<mxGeometry x="184" y="385" width="100" height="40" as="geometry"/>'+
        '</mxCell>'+
        '<mxCell id="64" class="ITF ITF3_N2" value="interfaz3" style="text;strokeColor=none;fillColor=none;html=1;fontSize=13;fontStyle=0;verticalAlign=middle;align=center;fontColor=#FF0000;" parent="1" vertex="1">'+
          '<mxGeometry x="156" y="158" width="100" height="40" as="geometry"/>'+
        '</mxCell>'+
        '<mxCell id="65" class="ITF ITF4_N2" value="interfaz4" style="text;strokeColor=none;fillColor=none;html=1;fontSize=13;fontStyle=0;verticalAlign=middle;align=center;fontColor=#FF0000;" parent="1" vertex="1">'+
          '<mxGeometry x="298" y="114" width="100" height="24" as="geometry"/>'+
        '</mxCell>'+
        '<mxCell id="66" class="ITF ITF5_N3" value="interfaz5" style="text;strokeColor=none;fillColor=none;html=1;fontSize=13;fontStyle=0;verticalAlign=middle;align=center;fontColor=#FF0000;" parent="1" vertex="1">'+
          '<mxGeometry x="379" y="158" width="100" height="29" as="geometry"/>'+
        '</mxCell>'+
        '<mxCell id="67" class="ITF ITF5_N3" value="interfaz6" style="text;strokeColor=none;fillColor=none;html=1;fontSize=13;fontStyle=0;verticalAlign=middle;align=center;fontColor=#FF0000;" parent="1" vertex="1">'+
          '<mxGeometry x="500" y="231" width="100" height="40" as="geometry"/>'+
        '</mxCell>'+
        '<mxCell id="68" class="ITF ITF7_N4" value="interfaz7" style="text;strokeColor=none;fillColor=none;html=1;fontSize=13;fontStyle=0;verticalAlign=middle;align=center;fontColor=#FF0000;" parent="1" vertex="1">'+
          '<mxGeometry x="465" y="365" width="100" height="40" as="geometry"/>'+
        '</mxCell>'+
        '<mxCell id="69" class="ITF ITF8_N4" value="interfaz8" style="text;strokeColor=none;fillColor=none;html=1;fontSize=13;fontStyle=0;verticalAlign=middle;align=center;fontColor=#FF0000;" parent="1" vertex="1">'+
          '<mxGeometry x="326" y="398" width="100" height="40" as="geometry"/>'+
        '</mxCell>'+
        '<mxCell id="74" class="Cloud" value="" style="shape=image;html=1;labelBackgroundColor=#ffffff;image=/edt/stencils/clipart/TestConection.gif;strokeColor=#FF0000;strokeWidth=1;fillColor=#FF0000;fontSize=13;fontColor=#FF0000;" parent="1" vertex="1">'+
          '<mxGeometry x="556" width="80" height="80" as="geometry"/>'+
        '</mxCell>'+
      '</root>'+
    '</mxGraphModel>'
  },
      {noAnillo: 5, xml:''},
      {noAnillo: 6, xml:''},
      {noAnillo: 7, xml:''},
      {noAnillo: 8, xml:''}
    ],
    anillos:[
      {
        id:1,
        anillo:'GDL_CANADAx',
        estatus:1,
        noNodos:0,
        version:1,
        nodos:[
          {
            equipo:'ROUTER 1',
            ip:'10.10.10.1',
            mpls:'10.10.20.1',
            VRRP: 'Virtual-Ethernet8/0/3.4085|Virtual-Ethernet1/0/3.4084',
            orden:'1',
            interfaz:'100GE8/0/0',
            down:'SI',
            LLDP: 'SI',
            conectaCon:'SWITCH 1',
            IP_Colector_ValidacionServicio_Datos_Ping: '10.10.10.85',
            IP_Lanwan_ValidacionServicio_Datos: '10.10.10.10',
            ipOlt:'10.0.0.1',
            posONT:'0/5/0 29',
            IP_Destino_Validacion_Internet_ONT: 'www.cisco.com',
            
          },
          {
            equipo:'SWITCH 1',
            ip:'10.10.10.2',
            mpls:'',
            VRRP: '',
            orden:'2',
            interfaz:'100GE1/0/1',
            down:'SI',
            LLDP: 'SI',
            conectaCon:'SWITCH 2',
            IP_Colector_ValidacionServicio_Datos_Ping: '10.10.10.86',
            IP_Lanwan_ValidacionServicio_Datos: '',
            ipOlt:'10.0.0.2',
            posONT:'0/1/5 1',
            IP_Destino_Validacion_Internet_ONT: 'www.cisco.com',
          },
          {
            equipo:'ROUTER 2',
            ip:'10.10.10.4',
            mpls:'10.10.20.2',
            VRRP: 'Virtual-Ethernet7/0/3.4085|Virtual-Ethernet1/1/3.4084',
            orden:'3',
            interfaz:'100GE7/0/0',
            down:'SI',
            LLDP: 'SI',
            conectaCon:'SWITCH 2',
            IP_Colector_ValidacionServicio_Datos_Ping: '',
            IP_Lanwan_ValidacionServicio_Datos: '',
            ipOlt:'',
            posONT:'',
            IP_Destino_Validacion_Internet_ONT: '',
          },
        ],
  
      },
    ],
    btnActivo:false,
    modoEdicion:false,
    editedNodo: {
      valNombreNodo:'',
      valIpEquipo:'',
      valMPLS:'',
      valVrrp:'',
      valOrden:'',
      valInterfaz:'',
      valDown:null,
      valLLDP:null,
      valConectaCon:'',
      valIpColector:'',
      valIpLANWAN:'',
      valIpOLT:'',
      valPosONT:'',
      valIpDestino:'',
    },
    nombreAnillo:'',
    nodosAnillo:[],
    indexAnilloEdit:-1,
  },
  mutations: {
  },
  actions: {
  },
  modules: {
  }
})
